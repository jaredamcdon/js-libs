# js libs

This repo is for reference, none is written by me but all are open source projects.

- [Bootstrap](https://getbootstrap.com/)
    - CSS Framework
- [Charts.js](https://www.chartjs.org/)
    - JS Framework (Visualization)
- [React](https://reactjs.org/)
    - Frontend JS Framework
- [Vue.js](https://vuejs.org/)
    - Frontend JS Framework
- [jQuery](https://jquery.com/)
    - JS Library for requests and events etc
